package storage

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitee.com/trced/goutils"
	"gitee.com/trced/goutils/logs"
	_ "github.com/go-sql-driver/mysql"
)

type DbCli struct {
	config    *DbConfig
	db        *sql.DB
	chanSql   chan string
	closeFlag bool
	wg        sync.WaitGroup
	sm        *SchemaManager
}

type DbConfig struct {
	StrAddr         string        // 连接串
	ConnMaxLifetime time.Duration // 连接超时时间
	MaxOpenConns    int           // 最大连接数
	MaxIdleConns    int           // 最大空闲连接数
	QueueLimitCount int           // sql队列上限数, 超过上限必需等待
}

// 打开一个数据库连接池
func newDbCli(cfg *DbConfig) (db *DbCli, err error) {
	var d *sql.DB
	d, err = sql.Open("mysql", cfg.StrAddr)
	if err != nil {
		logs.Error("mysql connection failed: %v %v", cfg.StrAddr, err)
		return nil, err
	}
	if err = d.Ping(); err != nil {
		d.Close()
		logs.Error("mysql connection failed: %v %v", cfg.StrAddr, err)
		return nil, err
	}
	if cfg.MaxIdleConns != 0 {
		d.SetMaxIdleConns(cfg.MaxIdleConns)
	}

	if cfg.MaxOpenConns != 0 {
		d.SetMaxOpenConns(cfg.MaxOpenConns)
	}

	if cfg.ConnMaxLifetime != 0 {
		d.SetConnMaxLifetime(cfg.ConnMaxLifetime)
	}

	db = &DbCli{
		config:    cfg,
		db:        d,
		closeFlag: false,
	}
	db.chanSql = make(chan string, cfg.QueueLimitCount)
	db.sm = newSchemaManager()

	go db.startQueueTask()

	logs.Info("mysql connection success: %v", cfg.StrAddr)

	return db, nil
}

// 启动sql队列执行任务
func (this *DbCli) startQueueTask() {
	this.wg.Add(1)
	defer this.wg.Done()

	for {
		select {
		case strSql := <-this.chanSql:
			if strSql == "" && len(this.chanSql) == 0 {
				logs.Info("[%v] queue stopped", this.config.StrAddr)
				return
			}
			_, err := this.Exec(strSql)
			if err != nil {
				logs.Error("%v", err)
			}
		}
	}
}

func (this *DbCli) Destroy() {
	if this.closeFlag == false {
		this.closeFlag = true
		this.chanSql <- ""
		logs.Info("[%v] waiting queue, count:%v", this.config.StrAddr, len(this.chanSql))
		this.wg.Wait()
		if this.db != nil {
			this.db.Close()
		}
	}
}

// 获取连接的数据库名
func (this *DbCli) CurrentDatabase() (name string) {
	strSql, err := CreateCurrentDatabaseSql()
	if err != nil {
		logs.Error(fmt.Sprintf("create sql error: %v", err))
		return
	}
	this.db.QueryRow(strSql).Scan(&name)
	return
}

// 获取数据库中的所有表名
func (this *DbCli) GetAllTableNames() ([]string, error) {
	strSql := CreateSelectTablesName(this.CurrentDatabase())
	logs.Debug("%v", strSql)

	rows, err := this.QueryRow(strSql)
	if err != nil {
		return nil, fmt.Errorf("sql error. %v, %v", strSql, err)
	}

	tableNames := make([]string, 0)
	for rows.Next() {
		var col1 string
		err = rows.Scan(&col1)
		if err != nil {
			return nil, fmt.Errorf("sql error. %v, %v", strSql, err)
		}
		tableNames = append(tableNames, col1)
	}

	return tableNames, nil
}

// 查询数据库中表结构
func (this *DbCli) GetTableStruct(tableName string) ([]*Field, error) {
	strSql := CreateSelectTableStruct(tableName)
	logs.Debug("%v", strSql)

	rows, err := this.QueryRow(strSql)
	if err != nil {
		return nil, fmt.Errorf("sql error. %v, %v", strSql, err)
	}

	fields := make([]*Field, 0)
	for rows.Next() {
		var col1 string
		var col2 string
		var col3 string
		var col4 string
		var col5 interface{}
		var col6 interface{}
		err = rows.Scan(&col1, &col2, &col3, &col4, &col5, &col6)
		if err != nil {
			return nil, fmt.Errorf("sql error. %v, %v", strSql, err)
		}
		f := Field{}
		f.Name = col1
		f.ColumnName = col1
		if idx := strings.Index(col2, "("); idx > 0 {
			f.ColumnType = EnumColumnType(goutils.SubString(col2, 0, strings.Index(col2, "(")))
			x, err := strconv.Atoi(goutils.SubString(col2, strings.Index(col2, "(")+1, strings.Index(col2, ")")))
			if err != nil {
				return nil, err
			}
			f.ColumnLength = int16(x)
		} else {
			f.ColumnType = EnumColumnType(col2)
			f.ColumnLength = 0
		}
		f.PrimaryKey = col4 == "PRI"

		fields = append(fields, &f)
	}

	return fields, nil
}

// 当前连接的数据库是否存在指定表
func (this *DbCli) HasTable(tableName string) (bool, error) {
	strSql, err := CreateHasTableSql(this.CurrentDatabase(), tableName)
	if err != nil {
		logs.Error(fmt.Sprintf("create sql error: %v", err))
		return false, err
	}
	logs.Debug("%v", strSql)
	var count int
	err = this.db.QueryRow(strSql).Scan(&count)
	if err != nil {
		logs.Error("db exec %v err:%v", strSql, err)
		return false, err
	}
	return count > 0, nil
}

// 获取表列中的最大值
func (this *DbCli) SelectMaxValue(tableName string, columnName string) (int64, error) {
	strSql, err := CreateColumnMaxValueSql(tableName, columnName)
	if err != nil {
		logs.Error(fmt.Sprintf("create sql error: %v", err))
		return 0, err
	}
	logs.Debug("%v", strSql)
	var count int64
	err = this.db.QueryRow(strSql).Scan(&count)
	if err != nil {
		logs.Error("db exec %v err:%v", strSql, err)
		return 0, err
	}
	return count, nil
}

// 当前连接的数据库表是否存在指定列
func (this *DbCli) HasColumn(tableName string, columnName string) (bool, error) {
	strSql, err := CreateHasColumnSql(this.CurrentDatabase(), tableName, columnName)
	if err != nil {
		logs.Error(fmt.Sprintf("Create sql error: %v", err))
		return false, err
	}
	logs.Debug("%v", strSql)
	var count int
	err = this.db.QueryRow(strSql).Scan(&count)
	if err != nil {
		logs.Error("db exec %v err:%v", strSql, err)
		return false, err
	}
	return count > 0, nil
}

// 执行sql语句
func (this *DbCli) Exec(query string, args ...interface{}) (sql.Result, error) {
	result, err := this.db.Exec(query, args...)
	if err != nil {
		return nil, fmt.Errorf("%v; %v", query, err)
	}
	logs.Debug("%v", query)
	return result, nil
}

// 查询多行数据
func (this *DbCli) QueryRow(query string, args ...interface{}) (*sql.Rows, error) {
	return this.db.Query(query, args...)
}

// 查询单行数据
func (this *DbCli) Query(query string, args ...interface{}) *sql.Row {
	row := this.db.QueryRow(query, args...)
	return row
}

// 查询单行数据, p: 结构实例指针 *mystruct{}, strSql=sql查询脚本
func (this *DbCli) SelectSingleBySql(p interface{}, strSql string) (err error) {
	schema, err := this.sm.GetSchema(p)
	if err != nil {
		return err
	}
	vContainer := GetValueContainer(schema)
	row := this.Query(strSql)
	if row != nil {
		err = row.Scan(vContainer...)
		if err != nil {
			return err
		}
		err = TransformRowData(schema, vContainer, p)
	}
	logs.Debug("%v;", strSql)
	return err
}

// 查询单行数据, p: 结构实例指针, params: 查询条件(列名-值)  p支持示例： *mystruct{}
func (this *DbCli) SelectSingle(p interface{}, params map[string]interface{}) (err error) {
	schema, err := this.sm.GetSchema(p)
	if err != nil {
		return err
	}

	var strSql string
	strSql, err = CreateSelectSql(schema, params)
	logs.Debug(strSql)
	if err != nil {
		return err
	}

	return this.SelectSingleBySql(p, strSql)
}

// 查询单行数据, p: 结构实例指针, where: 查询条件
func (this *DbCli) SelectSingleByWhere(p interface{}, where string) (err error) {
	schema, err := this.sm.GetSchema(p)
	if err != nil {
		return err
	}

	var strSql string
	strSql, err = CreateSelectSql(schema, nil)
	if err != nil {
		return err
	}

	strSql = fmt.Sprintf("%v where %v", strSql, where)

	return this.SelectSingleBySql(p, strSql)
}

// 查询多行数据, p: slice或map ,p支持示例： *[]mystruct{}  *[]*mystruct{}  []*mystruct{}  []mystruct{}, sql:sql语句
func (this *DbCli) SelectMultipleBySql(p interface{}, strSql string) (err error) {
	schema, err := this.sm.GetSchema(p)
	if err != nil {
		return err
	}

	vContainer := GetValueContainer(schema)
	rows, errQuery := this.QueryRow(strSql)
	if errQuery != nil {
		return fmt.Errorf("sql error. %v, %v", strSql, errQuery)
	}

	if rows != nil {
		structType := GetStructType(p)
		pContainerKind := reflect.TypeOf(p).Elem().Kind()
		pStructKind := reflect.TypeOf(p).Elem().Elem().Kind()

		results := reflect.ValueOf(p)
		if results.Kind() == reflect.Ptr {
			results = results.Elem()
		}
		for rows.Next() {
			err = rows.Scan(vContainer...)
			if err != nil {
				break
			}
			newItem := reflect.New(structType).Interface()
			err = TransformRowData(schema, vContainer, newItem)
			if err != nil {
				break
			}
			if pContainerKind == reflect.Map {
				firstField := reflect.ValueOf(newItem).Elem().FieldByName(schema.Fields[0].Name)
				if !firstField.IsValid() {
					return fmt.Errorf("%v get value Field '%v' ", reflect.TypeOf(p), schema.Fields[0].Name)
				}
				if pStructKind == reflect.Ptr {
					results.SetMapIndex(firstField, reflect.ValueOf(newItem))
				} else {
					results.SetMapIndex(firstField, reflect.ValueOf(newItem).Elem())
				}
			} else {
				if pStructKind == reflect.Ptr {
					results.Set(reflect.Append(results, reflect.ValueOf(newItem)))
				} else {
					results.Set(reflect.Append(results, reflect.ValueOf(newItem).Elem()))
				}
			}
		}
	}
	logs.Debug("%v;", strSql)
	return err
}

// 查询多行数据, p: slice或map, params: 查询条件(列名-值) p支持示例： *[]mystruct{}  *[]*mystruct{}
func (this *DbCli) SelectMultiple(p interface{}, params map[string]interface{}) (err error) {
	schema, err := this.sm.GetSchema(p)
	if err != nil {
		return err
	}
	var strSql string
	strSql, err = CreateSelectSql(schema, params)
	if err != nil {
		return err
	}

	return this.SelectMultipleBySql(p, strSql)
}

// 将sql语句放入队列
func (this *DbCli) PutToQueue(strSql string) {
	if this.closeFlag {
		logs.Error("cannot put in queue! db queue stopping: %v", strSql)
		return
	}
	this.chanSql <- strSql
}

// 异步插入数据
func (this *DbCli) AsyncInsert(p interface{}) {
	schema, err := this.sm.GetSchema(p)
	if err != nil {
		logs.Error(fmt.Sprintf("schema not exists: %v", p))
		return
	}

	arrSql, err := CreateInsertSql(schema, p)
	if err != nil {
		logs.Error(fmt.Sprintf("create sql error: %v", err))
		return
	}

	for _, v := range arrSql {
		this.PutToQueue(v)
	}
}

// 异步插入数据 有bug
// func (d *DbCli) SyncInsert(p interface{}) int64 {
// 	schema, err := d.sm.GetSchema(p)
// 	if err != nil {
// 		logs.Error(fmt.Sprintf("schema not exists: %v", p))
// 		return 0
// 	}

// 	arrSql, err := CreateInsertSql(schema, p)
// 	if err != nil {
// 		logs.Error(fmt.Sprintf("create sql error: %v", err))
// 		return 0
// 	}

// 	if len(arrSql) < 1 {
// 		logs.Error("sql length less than 1 %v", arrSql)
// 		return 0
// 	}

// 	res, err := d.Exec(arrSql[0])
// 	if err != nil {
// 		logs.Error("sql error %v", err)
// 		return 0
// 	}

// 	res2, err := res.LastInsertId()
// 	if err != nil {
// 		logs.Error("last insert id error %v", err)
// 		return 0
// 	}

// 	return res2
// }

// 异步更新数据
func (this *DbCli) AsyncUpdate(p interface{}, fields ...string) {
	schema, err := this.sm.GetSchema(p)
	if err != nil {
		logs.Error(fmt.Sprintf("schema not exists: %v", p))
		return
	}

	strSql, err := CreateUpdateSql(schema, p, fields...)
	if err != nil {
		logs.Error(fmt.Sprintf("create sql error: %v", err))
		return
	}
	this.PutToQueue(strSql)
}

// 异步删除数据
func (this *DbCli) AsyncDelete(p interface{}) {
	schema, err := this.sm.GetSchema(p)
	if err != nil {
		logs.Error(fmt.Sprintf("schema not exists: %v", p))
		return
	}

	strSql, err := CreateDeleteSql(schema, p)
	if err != nil {
		logs.Error(fmt.Sprintf("create sql error: %v", err))
		return
	}
	this.PutToQueue(strSql)
}

// 结构体管理
func (this *DbCli) GetSchemaManager() *SchemaManager {
	return this.sm
}

// 同步表结构
func (this *DbCli) syncTableStruct(hasTablesName []string, schema *Schema) {
	if goutils.ContainSVStr(hasTablesName, schema.TableName) {
		fields, err := this.GetTableStruct(schema.TableName)
		if err != nil {
			panic(err)
		}
		addColumnSqls := CreateTableAddColumnSql(schema, fields)
		for _, v := range addColumnSqls {
			_, err = this.Exec(v)
			if err != nil {
				panic(err)
			}
		}

		if len(addColumnSqls) > 0 {
			fields, err = this.GetTableStruct(schema.TableName)
			if err != nil {
				panic(err)
			}
		}
		modifyColumnSqls := CreateTableModifyColumnSql(schema, fields)
		for _, v := range modifyColumnSqls {
			_, err = this.Exec(v)
			if err != nil {
				panic(err)
			}
		}
	} else {
		strSql, err := CreateNewTableSql(schema)
		if err != nil {
			panic(errors.New(fmt.Sprintf("sync table struct error: %v", err)))
		}
		_, err = this.Exec(strSql)
		if err != nil {
			logs.Error(err)
			panic(err)
		}
	}
}

// 同步表结构
func (this *DbCli) SyncAllTableStruct() {
	hasTablesName, err := this.GetAllTableNames()
	if err != nil {
		panic(err)
	}
	for _, v := range this.sm.GetAllSchema() {
		this.syncTableStruct(hasTablesName, v)
	}
}

// 同步表结构
func (this *DbCli) SyncTableStruct(p ...interface{}) {
	if p == nil {
		return
	}

	hasTablesName, err := this.GetAllTableNames()
	if err != nil {
		panic(err)
	}

	for _, v := range p {
		s, err := this.sm.GetSchema(v)
		if err != nil {
			panic(err)
		}
		this.syncTableStruct(hasTablesName, s)
	}
}
