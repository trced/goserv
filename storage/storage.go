package storage

import (
	"errors"
	"fmt"
)

var storage *Storage

type Storage struct {
	redisClis map[int]*RedisCli
	dbClis    map[int]*DbCli
}

func init() {
	storage = &Storage{}
	storage.redisClis = map[int]*RedisCli{}
	storage.dbClis = map[int]*DbCli{}
}

func GetRedisCli() *RedisCli {
	return storage.redisClis[0]
}

func GetRedisCliExt(idx int) *RedisCli {
	return storage.redisClis[idx]
}

func GetDbCli() *DbCli {
	return storage.dbClis[0]
}

func GetDbCliExt(idx int) *DbCli {
	return storage.dbClis[idx]
}

func Destroy() {
	storage.Destroy()
}

func AddRedisCli(redisCliIdx int, redisCfg *RedisConfig) error {
	if _, ok := storage.redisClis[redisCliIdx]; ok {
		return errors.New(fmt.Sprintf("RedisCli idx %v has already existed", redisCliIdx))
	}

	redisCli, err := newRedisClipool(redisCfg)
	if err != nil {
		return err
	}
	storage.redisClis[redisCliIdx] = redisCli

	return nil
}

func AddDbCli(dbCliIdx int, dbCfg *DbConfig) error {
	if _, ok := storage.dbClis[dbCliIdx]; ok {
		return errors.New(fmt.Sprintf("DbCli idx %v has already existed", dbCliIdx))
	}

	dbCli, err := newDbCli(dbCfg)
	if err != nil {
		return err
	}
	storage.dbClis[dbCliIdx] = dbCli

	return nil
}

func (s *Storage) Destroy() {
	for _, redisCli := range s.redisClis {
		redisCli.Destroy()
	}
	for _, dbCli := range s.dbClis {
		dbCli.Destroy()
	}
}

// 从Redis和Db中添加数据
// key 表名
// field 主键值
// p 数据
func Add(key string, field interface{}, p interface{}) (err error) {
	redisCli := GetRedisCli()
	if redisCli == nil {
		err = errors.New(fmt.Sprintf("Add: redisCli idx %v not exists", 0))
		return
	}

	err = redisCli.DoHSetExt(key, field, p)
	if err != nil {
		return
	}

	dbCli := GetDbCli()
	if dbCli == nil {
		err = errors.New(fmt.Sprintf("Add: dbCli idx %v not exists", 0))
		return
	}
	dbCli.AsyncInsert(p)

	return nil
}

func AddWDbcli(dbCli *DbCli, key string, field interface{}, p interface{}) (err error) {
	redisCli := GetRedisCli()
	if redisCli == nil {
		err = errors.New(fmt.Sprintf("Add: redisCli idx %v not exists", 0))
		return
	}

	err = redisCli.DoHSetExt(key, field, p)
	if err != nil {
		return
	}

	// dbCli := GetDbCli()
	if dbCli == nil {
		err = errors.New(fmt.Sprintf("Add: dbCli idx %v not exists", 0))
		return
	}
	dbCli.AsyncInsert(p)

	return nil
}

// 从Redis和Db中删除数据
func Delete(key string, field interface{}, p interface{}) (err error) {
	redisCli := GetRedisCli()
	if redisCli == nil {
		err = errors.New(fmt.Sprintf("Delete: redisCli idx %v not exists", 0))
		return
	}

	err = redisCli.DoHDelExt(key, field)
	if err != nil {
		return
	}

	dbCli := GetDbCli()
	if dbCli == nil {
		err = errors.New(fmt.Sprintf("Delete: dbCli idx %v not exists", 0))
		return
	}
	dbCli.AsyncDelete(p)

	return nil
}

// 从Redis和Db中更新数据
// key 表名
// field 主键
// p 数据
// fields 要更新的属性
func Update(key string, field interface{}, p interface{}, fields ...string) (err error) {
	redisCli := GetRedisCli()
	if redisCli == nil {
		err = errors.New(fmt.Sprintf("Update: redisCli idx %v not exists", 0))
		return
	}

	err = redisCli.DoHSetExt(key, field, p)
	if err != nil {
		return
	}

	dbCli := GetDbCli()
	if dbCli == nil {
		err = errors.New(fmt.Sprintf("Update: dbCli idx %v not exists", 0))
		return
	}
	dbCli.AsyncUpdate(p, fields...)

	return nil
}

// 从Redis和Db中更新数据
// dbCli mysql 连接
// key 表名
// field 主键
// p 数据
// fields 要更新的属性
func UpdateDbCli(dbCli *DbCli, key string, field interface{}, p interface{}, fields ...string) (err error) {
	redisCli := GetRedisCli()
	if redisCli == nil {
		err = errors.New(fmt.Sprintf("Update: redisCli idx %v not exists", 0))
		return
	}

	err = redisCli.DoHSetExt(key, field, p)
	if err != nil {
		return
	}

	// dbCli := GetDbCli()
	if dbCli == nil {
		err = errors.New(fmt.Sprintf("Update: dbCli idx %v not exists", 0))
		return
	}
	dbCli.AsyncUpdate(p, fields...)

	return nil
}

// 从Redis和Db中更新数据
func UpdateMultiple(key string, args map[interface{}]interface{}, fields ...string) (err error) {
	redisCli := GetRedisCli()
	if redisCli == nil {
		err = errors.New(fmt.Sprintf("UpdateMultiple: redisCli idx %v not exists", 0))
		return
	}

	err = redisCli.DoHMSetExt(key, args)
	if err != nil {
		return
	}

	dbCli := GetDbCli()
	if dbCli == nil {
		err = errors.New(fmt.Sprintf("UpdateMultiple: dbCli idx %v not exists", 0))
		return
	}
	for _, v := range args {
		dbCli.AsyncUpdate(v, fields...)
	}

	return nil
}

// 从Redis和Db中添加数据
func AddMultiple(key string, args map[interface{}]interface{}) (err error) {
	redisCli := GetRedisCli()
	if redisCli == nil {
		err = errors.New(fmt.Sprintf("AddMultiple: redisCli idx %v not exists", 0))
		return
	}

	err = redisCli.DoHMSetExt(key, args)
	if err != nil {
		return
	}

	dbCli := GetDbCli()
	if dbCli == nil {
		err = errors.New(fmt.Sprintf("AddMultiple: dbCli idx %v not exists", 0))
		return
	}
	for _, v := range args {
		dbCli.AsyncInsert(v)
	}

	return nil
}

// 从Redis和Db中删除数据
func DeleteMultiple(key string, args map[interface{}]interface{}) (err error) {
	redisCli := GetRedisCli()
	if redisCli == nil {
		err = errors.New(fmt.Sprintf("DeleteMultiple: redisCli idx %v not exists", 0))
		return
	}

	delIds := make([]interface{}, 0, len(args))
	for k := range args {
		delIds = append(delIds, k)
	}

	err = redisCli.DoHDelExt(key, delIds...)
	if err != nil {
		return
	}

	dbCli := GetDbCli()
	if dbCli == nil {
		err = errors.New(fmt.Sprintf("DeleteMultiple: dbCli idx %v not exists", 0))
		return
	}
	for _, v := range args {
		dbCli.AsyncDelete(v)
	}

	return nil
}

// 从Redis和Db中添加数据
func ZAdd(key string, score int64, member interface{}, p interface{}) (err error) {
	redisCli := GetRedisCli()
	if redisCli == nil {
		err = errors.New(fmt.Sprintf("ZAdd: redisCli idx %v not exists", 0))
		return
	}

	err = redisCli.DoZAdd(key, score, member)
	if err != nil {
		return
	}

	dbCli := GetDbCli()
	if dbCli == nil {
		err = errors.New(fmt.Sprintf("ZAdd: dbCli idx %v not exists", 0))
		return
	}
	dbCli.AsyncInsert(p)

	return nil
}

// 从Redis和Db中更新数据
func ZUpdate(key string, score int64, member interface{}, p interface{}, fields ...string) (err error) {
	redisCli := GetRedisCli()
	if redisCli == nil {
		err = errors.New(fmt.Sprintf("ZUpdate: redisCli idx %v not exists", 0))
		return
	}

	err = redisCli.DoZAdd(key, score, member)
	if err != nil {
		return
	}

	dbCli := GetDbCli()
	if dbCli == nil {
		err = errors.New(fmt.Sprintf("Update: dbCli idx %v not exists", 0))
		return
	}
	dbCli.AsyncUpdate(p, fields...)

	return nil
}
