package gate

import (
	"fmt"
	"gameserver/msgnew"
	"net"
	"reflect"
	"time"

	"github.com/golang/protobuf/proto"

	"gitee.com/trced/goserv/chanrpc"
	"gitee.com/trced/goserv/network"
	"gitee.com/trced/goutils/logs"
)

type Gate struct {
	MaxConnNum      int
	PendingWriteNum int
	MaxMsgLen       uint32
	Processor       network.Processor
	AgentChanRPC    *chanrpc.Server

	// websocket
	WSAddr      string
	HTTPTimeout time.Duration
	CertFile    string
	KeyFile     string

	// tcp
	TCPAddr      string
	LenMsgLen    int
	LittleEndian bool
}

func (gate *Gate) Run(closeSig chan bool) {
	var wsServer *network.WSServer
	if gate.WSAddr != "" {
		wsServer = new(network.WSServer)
		wsServer.Addr = gate.WSAddr
		wsServer.MaxConnNum = gate.MaxConnNum
		wsServer.PendingWriteNum = gate.PendingWriteNum
		wsServer.MaxMsgLen = gate.MaxMsgLen
		wsServer.HTTPTimeout = gate.HTTPTimeout
		wsServer.CertFile = gate.CertFile
		wsServer.KeyFile = gate.KeyFile
		wsServer.NewAgent = func(conn *network.WSConn) network.Agent {
			a := &agent{conn: conn, gate: gate}
			if gate.AgentChanRPC != nil {
				gate.AgentChanRPC.Go("NewAgent", a)
			}
			return a
		}
	}

	var tcpServer *network.TCPServer
	if gate.TCPAddr != "" {
		tcpServer = new(network.TCPServer)
		tcpServer.Addr = gate.TCPAddr
		tcpServer.MaxConnNum = gate.MaxConnNum
		tcpServer.PendingWriteNum = gate.PendingWriteNum
		tcpServer.LenMsgLen = gate.LenMsgLen
		tcpServer.MaxMsgLen = gate.MaxMsgLen
		tcpServer.LittleEndian = gate.LittleEndian
		tcpServer.NewAgent = func(conn *network.TCPConn) network.Agent {
			a := &agent{conn: conn, gate: gate}
			if gate.AgentChanRPC != nil {
				gate.AgentChanRPC.Go("NewAgent", a)
			}
			return a
		}
	}

	if wsServer != nil {
		wsServer.Start()
		logs.Info("Game WS Service startup: %v", wsServer.Addr)
	}
	if tcpServer != nil {
		tcpServer.Start()
		logs.Info("Game TCP Service startup: %v", tcpServer.Addr)
	}
	<-closeSig
	if wsServer != nil {
		wsServer.Close()
		logs.Info("Game WS Service stopped: %v", wsServer.Addr)
	}
	if tcpServer != nil {
		tcpServer.Close()
		logs.Info("Game TCP Service stopped: %v", tcpServer.Addr)
	}
}

func (gate *Gate) OnDestroy() {}

// 代表一个客户端
type agent struct {
	conn     network.Conn
	gate     *Gate
	userData interface{}
}

func (a *agent) Run() {
	for {
		data, err := a.conn.ReadMsg()
		if err != nil {
			break
		}
		if a.gate.Processor != nil {
			msg, err := a.gate.Processor.Unmarshal(data)
			if err != nil {
				logs.Debug("unmarshal message error: %v", err)
				break
			}
			err = a.gate.Processor.Route(msg, a)
			if err != nil {
				logs.Debug("route message error: %v", err)
				break
			}
		}
	}
}

func (a *agent) OnClose() {
	if a.gate.AgentChanRPC != nil {
		// eventsnew.Events.Trigger(eventsnew.EventsRoleLoginOut, a.userData.(*global.AgentInfo))
		err := a.gate.AgentChanRPC.Call0("CloseAgent", a)
		if err != nil {
			logs.Error("chanrpc error: %v", err)
		}
	}
}

func (a *agent) WriteMsg(message interface{}) {
	if a.gate.Processor != nil {
		logs.Debug("send msg %v to agent %v", reflect.TypeOf(message), a.conn.RemoteAddr())
		aa, ab := message.(*msgnew.S2C_ERROR)
		if ab {
			logs.Debug("error message %v", aa)
		}

		data, err := a.gate.Processor.Marshal(message)
		if err != nil {
			logs.Error("marshal message %v error: %v", reflect.TypeOf(message), err)
			return
		}
		err = a.conn.WriteMsg(data...) // data...：data是两维切片，...用于解切片
		if err != nil {
			logs.Error("write message %v error: %v", reflect.TypeOf(message), err)
		}
	} else {
		logs.Warn("agent.gate.Processor is nil %v", a)
	}
}

func (a *agent) LocalAddr() net.Addr {
	return a.conn.LocalAddr()
}

func (a *agent) RemoteAddr() net.Addr {
	return a.conn.RemoteAddr()
}

func (a *agent) Close() {
	a.conn.Close()
}

func (a *agent) Destroy() {
	a.conn.Destroy()
}

func (a *agent) UserData() interface{} {
	return a.userData
}

func (a *agent) SetUserData(data interface{}) {
	a.userData = data
}

// MsgId (YYZ)
func (a *agent) MsgId(msg interface{}) uint32 {
	if a.gate.Processor == nil {
		panic("processor not init")
	}

	id, ok := a.gate.Processor.GetId(reflect.TypeOf(msg))
	if !ok {
		a.gate.Processor.Register(msg.(proto.Message))
		id, ok = a.gate.Processor.GetId(reflect.TypeOf(msg))
		if !ok {
			panic(fmt.Sprintf("un-registered message type:%v", reflect.TypeOf(msg).Name()))
		}
	}

	return id
}
