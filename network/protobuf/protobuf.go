package protobuf

import (
	"encoding/binary"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"sync"

	"gitee.com/trced/goserv/chanrpc"
	"gitee.com/trced/goutils/logs"
	"github.com/golang/protobuf/proto"
)

// -------------------------
// | id | protobuf message |
// -------------------------
type Processor struct {
	littleEndian bool
	msgInfo      map[uint32]*MsgInfo
	msgID        map[reflect.Type]uint32
	rwLock       sync.RWMutex
}

// router 与 handler 是不同的注册方式
// 但是功能近似，都是处理消息
// handler 的优先级比 router 高
type MsgInfo struct {
	msgType       reflect.Type
	msgRouter     *chanrpc.Server // 该消息的 router
	msgHandler    MsgHandler      //该消息的 handler
	msgRawHandler MsgHandler      // 目前为空
}

type MsgHandler func([]interface{})

type MsgRaw struct {
	msgID      uint32
	msgRawData []byte
}

func NewProcessor() *Processor {
	p := new(Processor)
	p.littleEndian = false
	p.msgInfo = make(map[uint32]*MsgInfo)
	p.msgID = make(map[reflect.Type]uint32)
	return p
}

// It's dangerous to call the method on routing or marshaling (unmarshaling)
func (p *Processor) SetByteOrder(littleEndian bool) {
	p.littleEndian = littleEndian
}

// It's dangerous to call the method on routing or marshaling (unmarshaling)
func (p *Processor) Register(msg proto.Message) uint32 {
	p.rwLock.Lock()

	msgType := reflect.TypeOf(msg)
	if msgType == nil || msgType.Kind() != reflect.Ptr {
		logs.Error("protobuf message pointer required")
	}
	if _, ok := p.msgID[msgType]; ok {
		logs.Error("message %s is already registered", msgType)
	}

	id := p.getMessageId(msgType)

	if _, ok := p.msgInfo[id]; ok {
		logs.Error("protobuf messages id duplicate name=%s id=%v", msgType.Name(), id)
	}

	i := new(MsgInfo)
	i.msgType = msgType
	p.msgInfo[id] = i
	p.msgID[msgType] = id

	p.rwLock.Unlock()
	return id
}

func (p *Processor) GetId(msgType reflect.Type) (ret uint32, ok bool) {
	p.rwLock.RLock()
	ret, ok = p.msgID[msgType]
	p.rwLock.RUnlock()
	return
}

func (p *Processor) GetInfo(id uint32) (ret *MsgInfo, ok bool) {
	p.rwLock.RLock()
	ret, ok = p.msgInfo[id]
	p.rwLock.RUnlock()
	return
}

// It's dangerous to call the method on routing or marshaling (unmarshaling)
func (p *Processor) SetRouter(msg proto.Message, msgRouter *chanrpc.Server) {
	msgType := reflect.TypeOf(msg)
	id, ok := p.GetId(msgType)
	if !ok {
		p.Register(msg)
		id, ok = p.GetId(msgType)
		if !ok {
			logs.Fatal("message %s not registered", msgType)
		}
	}

	msgInfo, ok := p.GetInfo(id)
	if ok {
		msgInfo.msgRouter = msgRouter
	}
}

// It's dangerous to call the method on routing or marshaling (unmarshaling)
//func (p *Processor) SetHandler(msg proto.Message, msgHandler MsgHandler) {
//	msgType := reflect.TypeOf(msg)
//	id, ok := p.msgID[msgType]
//	if !ok {
//		logs.Fatal("message %s not registered", msgType)
//	}
//
//	p.msgInfo[id].msgHandler = msgHandler
//}

// It's dangerous to call the method on routing or marshaling (unmarshaling)
func (p *Processor) SetRawHandler(id uint32, msgRawHandler MsgHandler) {
	pInfo, exist := p.GetInfo(id)
	if !exist {
		logs.Fatal("message id %v not registered", id)
	}

	pInfo.msgRawHandler = msgRawHandler
}

// goroutine safe
func (p *Processor) Route(msg interface{}, userData interface{}) error {
	// raw
	if msgRaw, ok := msg.(MsgRaw); ok {
		pInfo, exist := p.GetInfo(msgRaw.msgID)
		if !exist {
			return fmt.Errorf("message id %v not registered", msgRaw.msgID)
		}

		if pInfo.msgRawHandler != nil {
			pInfo.msgRawHandler([]interface{}{msgRaw.msgID, msgRaw.msgRawData, userData})
		}
		return nil
	}

	// protobuf
	msgType := reflect.TypeOf(msg)
	id, ok := p.GetId(msgType)
	if !ok {
		return fmt.Errorf("message %s not registered", msgType)
	}
	i, infoOk := p.GetInfo(id)
	if !infoOk {
		return fmt.Errorf("message %s not registered", msgType)
	}
	if i.msgHandler != nil {
		i.msgHandler([]interface{}{msg, userData})
	}
	if i.msgRouter != nil {
		i.msgRouter.Go(msgType, msg, userData)
	}
	return nil
}

// goroutine safe
// 如果存在与 msgId 对应的 msgType 则返回接口，否则返回 raw
func (p *Processor) Unmarshal(data []byte) (interface{}, error) {
	if len(data) < 4 {
		return nil, errors.New("protobuf data too short")
	}

	// id
	var id uint32
	if p.littleEndian {
		id = binary.LittleEndian.Uint32(data)
	} else {
		id = binary.BigEndian.Uint32(data)
	}

	i, exist := p.GetInfo(id)
	if !exist {
		return nil, fmt.Errorf("message id %v not registered", id)
	}

	if i.msgRawHandler != nil {
		return MsgRaw{id, data[4:]}, nil
	} else {
		msg := reflect.New(i.msgType.Elem()).Interface()
		return msg, proto.UnmarshalMerge(data[4:], msg.(proto.Message))
	}
}

// goroutine safe
func (p *Processor) Marshal(msg interface{}) ([][]byte, error) {
	msgType := reflect.TypeOf(msg)

	// id
	_id, ok := p.GetId(msgType)
	if !ok {
		p.Register(msg.(proto.Message))
		_id, ok = p.GetId(msgType)
		if !ok {
			err := fmt.Errorf("message %s not registered", msgType)
			return nil, err
		}
	}

	id := make([]byte, 4)
	if p.littleEndian {
		binary.LittleEndian.PutUint32(id, _id)
	} else {
		binary.BigEndian.PutUint32(id, _id)
	}

	data, err := proto.Marshal(msg.(proto.Message))
	// 4位id，n位数据，4位id表示数据类型，id生成方式如下：getMessageId(reflect.Type)
	return [][]byte{id, data}, err
}

// goroutine safe
//func (p *Processor) Range(f func(id uint32, t reflect.Type)) {
//	for id, i := range p.msgInfo {
//		f(uint32(id), i.msgType)
//	}
//}

func (p *Processor) getMessageId(msgType reflect.Type) uint32 {
	arrMsgNameRes := strings.Split(msgType.String(), ".") // s := strings.Split("a.b.c.d.c2srequest",".")
	return p.hash(arrMsgNameRes[len(arrMsgNameRes)-1])    // hash("c2srequest")
}

// hash 算法，与客户端保持一致
// 根据 proto 的 message name 进行计算
func (p *Processor) hash(str string) uint32 {
	arrByte := []byte(str)
	var nHash uint32 = 1315423911
	for _, c := range arrByte {
		nHash ^= (nHash << 5) + uint32(c) + (nHash >> 2)
	}
	return nHash & 0x7FFFFFFF
}
