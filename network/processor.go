package network

import (
	"github.com/golang/protobuf/proto"
	"reflect"
)

type Processor interface {
	// must goroutine safe
	Route(msg interface{}, userData interface{}) error
	// must goroutine safe
	Unmarshal(data []byte) (interface{}, error)
	// must goroutine safe
	Marshal(msg interface{}) ([][]byte, error)

	Register(msg proto.Message) uint32

	GetId(msgType reflect.Type) (ret uint32, ok bool)
}
