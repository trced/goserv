package goserv

import (
	"os"
	"os/signal"

	"gitee.com/trced/goserv/cluster"
	"gitee.com/trced/goserv/console"
	"gitee.com/trced/goserv/module"
	"gitee.com/trced/goserv/storage"
	"gitee.com/trced/goutils/logs"
)

func Run(mods ...module.Module) {
	logs.Info("Gserv starting up ")

	// module
	for i := 0; i < len(mods); i++ {
		module.Register(mods[i])
	}
	module.Init()

	// cluster
	cluster.Init()

	// console
	console.Init()

	// close
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)
	sig := <-c
	logs.Info("Gserv closing down (signal: %v)", sig)
	Stop()
}

func Stop() {
	console.Destroy()
	cluster.Destroy()
	module.Destroy()
	storage.Destroy()
}
